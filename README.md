# Open Tibia Classic - Netstring

Library for reading and writing modified [netstrings](https://en.wikipedia.org/wiki/Netstring).

## Modifications

- allows for an optional leading '0x' to indicate the header is an ascii hex string (always prepends this from the Writer)
- terminates netstring on a newline '\n' instead of a ','