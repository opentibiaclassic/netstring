package net.opentibiaclassic.netstring;

import java.io.OutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;
import java.nio.charset.StandardCharsets;

public class NetstringWriter implements AutoCloseable {

    public static byte[] marshal(final String message) {
        return String.format("0x%s:%s\n",Integer.toHexString(message.length()), message).getBytes(StandardCharsets.UTF_8);
    }

    private final OutputStream outputStream;
    public NetstringWriter(final OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public NetstringWriter(final Socket socket) throws IOException {
        this(socket.getOutputStream());
    }

    public void write(final String message) throws IOException {
        this.outputStream.write(marshal(message));
    }

    public void flush() throws IOException {
        this.outputStream.flush();
    }

    @Override
    public void close() {
        try {
            this.outputStream.close();
        } catch(final Exception ignored) {}
    }
}