package net.opentibiaclassic.netstring;

import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.List;

public class NetstringReader implements AutoCloseable {

    private static class Const {
        public static final char START_OF_PREFIX = '0';
        public static final byte END_OF_PREFIX = (byte)'x';
        public static final List<Byte> VALID_PREFIX_END_CHARACTERS = List.of(
            END_OF_PREFIX,
            (byte)Character.toUpperCase((char)END_OF_PREFIX)
        );
        public static final int RADIX = 16;
        public static final byte END_OF_HEADER = (byte)':';
        public static final byte END_OF_NETSTRING = (byte)'\n';
        // TODO make header length/size configurable on constructor
        public static final int MAX_BODY_LENGTH_BYTES = 1048576;
        public static final int LENGTH_OF_HEADER_PREFIX_ASCII_CHARACTERS = 2;
        public static final int MAX_HEADER_LENGTH_ASCII_CHARACTERS = Integer.toHexString(MAX_BODY_LENGTH_BYTES).length();

    }

    public class InvalidTransitionException extends RuntimeException {
        public InvalidTransitionException(final ReadingState sourceState, final ReadingState destinationState) {
            super(String.format("Invalid ReadingState transition (%s -> %s)", sourceState, destinationState));
        }
    }

    private static enum ReadingState {
        READING_PREFIX_0,
        READING_PREFIX_1,
        READING_HEADER,
        READING_BODY,
        SKIPPING
    }

    private static final Map<ReadingState, List> validTransitions = Map.of(
        ReadingState.READING_PREFIX_0, List.of(ReadingState.READING_PREFIX_1, ReadingState.SKIPPING),
        ReadingState.READING_PREFIX_1, List.of(ReadingState.READING_HEADER, ReadingState.READING_BODY, ReadingState.SKIPPING),
        ReadingState.READING_HEADER, List.of(ReadingState.READING_BODY, ReadingState.SKIPPING),
        ReadingState.READING_BODY, List.of(ReadingState.SKIPPING),
        ReadingState.SKIPPING, List.of(ReadingState.READING_PREFIX_0)
    );

    private final InputStream inputStream;

    private ReadingState state;
    private final char[] prefix;
    private StringBuilder header;
    private byte[] body;
    private int offset;

    public NetstringReader(final InputStream inputStream) {
        this.inputStream = inputStream;

        this.prefix = new char[Const.LENGTH_OF_HEADER_PREFIX_ASCII_CHARACTERS];
        this.prefix[0] = 0;
        this.prefix[1] = 0;

        this.offset = 0;

        this.state = ReadingState.READING_PREFIX_0;
    }

    public NetstringReader(final Socket socket) throws IOException {
        this(socket.getInputStream());
    }

    @Override
    public void close() {
        try {
            this.inputStream.close();
        } catch(final Exception ignored) {}
    }

    private boolean isValidTransition(final ReadingState sourceState, final ReadingState destinationState) {
        return validTransitions.get(sourceState).contains(destinationState);
    }

    private void validateTransition(final ReadingState sourceState, final ReadingState destinationState) {
        if (!isValidTransition(sourceState, destinationState)) {
            throw new InvalidTransitionException(sourceState, destinationState);
        }
    }

    private void setState(final ReadingState newState) {
        validateTransition(this.state, newState);

        switch (newState) {
            case READING_PREFIX_0:
                this.body = null;
                this.prefix[0] = 0;
                this.prefix[1] = 0;
                break;

            case READING_HEADER:
                this.header = new StringBuilder();
                break;

            case READING_BODY:
                this.header = null;
                this.offset = 0;
                break;

            case SKIPPING:
                this.header = null;
                break;
        }

        this.state = newState;
    }

    private boolean isDoneReadingBody() {
        return this.offset == this.body.length;
    }

    private byte readByte() throws IOException, EOFException, SocketTimeoutException {
        final int read = inputStream.read();

        if (-1 == read) {
            throw new EOFException();
        }

        return (byte)read;
    }

    public String read() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        // preemptable blocking read state machine
        byte b;
        while(true) {
            switch(this.state) {
                case READING_PREFIX_0:
                    b = readByte();
                        if (-1 == Character.digit(b,Const.RADIX)) {
                            this.setState(ReadingState.SKIPPING);
                        } else {
                            this.prefix[0] = (char)b;
                            this.setState(ReadingState.READING_PREFIX_1);
                        }
                    break;

                case READING_PREFIX_1:
                    b = readByte();
                    if (Const.END_OF_HEADER == b) {
                        final int size = Integer.parseInt(Character.toString(this.prefix[0]), Const.RADIX);
                        if (0 == size) {
                            this.setState(ReadingState.SKIPPING);
                            return "";
                        }

                        this.body = new byte[size];
                        this.setState(ReadingState.READING_BODY);
                    } else if (Const.VALID_PREFIX_END_CHARACTERS.contains(b)) {
                        if (Const.START_OF_PREFIX != prefix[0]) {
                            this.setState(ReadingState.SKIPPING);
                        } else {
                            this.prefix[1] = (char)b;
                            this.setState(ReadingState.READING_HEADER);
                        }
                    } else if (-1 == Character.digit(b, Const.RADIX)) {
                        this.setState(ReadingState.SKIPPING);
                    } else {
                        this.prefix[1] = (char)b;

                        this.setState(ReadingState.READING_HEADER);
                        this.header.append(this.prefix);
                    }

                    break;

                case READING_HEADER:
                    b = readByte();

                    if (Const.END_OF_HEADER == b) {
                        final String h = this.header.toString();
                        final int size = Integer.parseInt(h, Const.RADIX);

                        if (0 == size) {
                            this.setState(ReadingState.SKIPPING);
                            return "";
                        }

                        body = new byte[size];
                        this.setState(ReadingState.READING_BODY);
                    } else if (Const.MAX_HEADER_LENGTH_ASCII_CHARACTERS == this.header.length()) {
                        this.setState(ReadingState.SKIPPING);
                    } else {
                        this.header.append((char)b);
                    }

                    break;

                case READING_BODY:
                    int read = inputStream.read(this.body, this.offset, this.body.length - this.offset);
                    if (-1 == read) {
                        throw new EOFException();
                    }

                    this.offset += read;

                    if (isDoneReadingBody()) {
                        this.setState(ReadingState.SKIPPING);
                        return new String(this.body, StandardCharsets.UTF_8);
                    }

                    break;

                case SKIPPING:
                    b = readByte();

                    if (Const.END_OF_NETSTRING == b) {
                        this.setState(ReadingState.READING_PREFIX_0);
                    }

                    break;
            }
        }
    }
}
